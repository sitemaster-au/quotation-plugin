<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       https://ridwan-arifandi.com
 * @since      1.0.0
 *
 * @package    Qmquote
 * @subpackage Qmquote/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    Qmquote
 * @subpackage Qmquote/includes
 * @author     Ridwan Arifandi <orangerdigiart@gmail.com>
 */
class Qmquote {

	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      Qmquote_Loader    $loader    Maintains and registers all hooks for the plugin.
	 */
	protected $loader;

	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $plugin_name    The string used to uniquely identify this plugin.
	 */
	protected $plugin_name;

	/**
	 * The current version of the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $version    The current version of the plugin.
	 */
	protected $version;

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the admin area and
	 * the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function __construct() {
		if ( defined( 'QMQUOTE_VERSION' ) ) {
			$this->version = QMQUOTE_VERSION;
		} else {
			$this->version = '1.0.0';
		}
		$this->plugin_name = 'qmquote';

		$this->load_dependencies();
		$this->set_locale();
		$this->define_admin_hooks();
		$this->define_public_hooks();

	}

	/**
	 * Load the required dependencies for this plugin.
	 *
	 * Include the following files that make up the plugin:
	 *
	 * - Qmquote_Loader. Orchestrates the hooks of the plugin.
	 * - Qmquote_i18n. Defines internationalization functionality.
	 * - Qmquote_Admin. Defines all hooks for the admin area.
	 * - Qmquote_Public. Defines all hooks for the public side of the site.
	 *
	 * Create an instance of the loader which will be used to register the hooks
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function load_dependencies() {

		/**
		 * The class responsible for orchestrating the actions and filters of the
		 * core plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-qmquote-loader.php';

		/**
		 * The class responsible for defining internationalization functionality
		 * of the plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-qmquote-i18n.php';

		/**
		 * The class responsible for defining all actions that occur in the admin area.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/admin.php';
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/rest.php';
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/quotation.php';
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/connection.php';
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/request.php';
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/service.php';

		/**
		 * The class responsible for defining all actions that occur in the public-facing
		 * side of the site.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/public.php';
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/shortcode.php';
		//       require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/request.php';
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/service.php';
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/calculation.php';
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/quotation.php';

		$this->loader = new Qmquote_Loader();

	}

	/**
	 * Define the locale for this plugin for internationalization.
	 *
	 * Uses the Qmquote_i18n class in order to set the domain and to register the hook
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function set_locale() {

		$plugin_i18n = new Qmquote_i18n();

		$this->loader->add_action( 'plugins_loaded', $plugin_i18n, 'load_plugin_textdomain' );

	}

	/**
	 * Register all of the hooks related to the admin area functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_admin_hooks() {

		$admin = new QMQUOTE\Admin( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'admin_enqueue_scripts', 		$admin, 'enqueue_styles' );
		$this->loader->add_action( 'admin_enqueue_scripts', 		$admin, 'enqueue_scripts' );
		$this->loader->add_action( 'after_setup_theme',				$admin, 'load_carbon_fields');
		$this->loader->add_action( 'carbon_fields_register_fields', $admin, 'set_plugin_options');
		//$this->loader->add_filter( 'plugin_row_meta',				$admin, 'add_plugin_info',	10,2);

		$rest = new QMQUOTE\Admin\Rest();

		$this->loader->add_action( 'init',							$rest, 'register_rewrite_url',	1);
		$this->loader->add_filter( 'query_vars',					$rest, 'register_query_vars',	999);
		$this->loader->add_action( 'template_redirect',				$rest, 'check_request_url',		1);

		$quotation = new QMQUOTE\Admin\Quotation( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'init'							,$quotation,	'register_post_type',999);
		$this->loader->add_action( 'add_meta_boxes'					,$quotation, 	'set_metaboxes'		,999);
		$this->loader->add_action( 'admin_enqueue_scripts'			,$quotation,	'enqueue_scripts',999);

		$connection = new QMQUOTE\Admin\Connection( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'admin_init'						,$connection, 	'check_token'			,999);
		$this->loader->add_action( 'qmquote/connection/check-token'	,$connection, 	'check_transient_token'	,999);
		$this->loader->add_action( 'admin_notices'					,$connection, 	'display_admin_message'	,999);

		$request = new QMQUOTE\Admin\Request( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'qmquote/request/create-quotation',	$request,	'create_quotation',	999,2);
		$this->loader->add_filter( 'qmquote/connection/messages',		$request, 	'set_messages',		999);

		$service = new QMQUOTE\Admin\Service( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'quoteme/rest/service/update-data',	$service,	'update_json_via_rest',		999);
		$this->loader->add_action( 'wp_ajax_request_update_service',	$service,	'update_json_via_ajax',		999);
		$this->loader->add_action( 'admin_init',						$service, 	'check_if_setting_page',	999);
		$this->loader->add_action( 'admin_enqueue_scripts',				$service,	'set_css_js_scripts',		999);
		$this->loader->add_action( 'admin_notices',						$service,	'show_update_dialog',		999);
	}

	/**
	 * Register all of the hooks related to the public-facing functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_public_hooks() {

		$public = new QMQUOTE\Front( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'wp_enqueue_scripts',	$public, 	'set_css_js_scripts',	999999);

		$shortcode = new QMQUOTE\Front\Shortcode( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'template_redirect',				$shortcode,	'check_shortcode'		,999);
		$this->loader->add_filter( 'qmquote/shortcode/status',		$shortcode, 'does_shortcode_exist'	,999);
		$this->loader->add_action( 'wp_enqueue_scripts',			$shortcode, 'set_css_js_scripts'	,1000000);
		$this->loader->add_action( 'quoteme/rest/form/display',		$shortcode, 'display_form'			,1);

		// $request = new QMQUOTE\Front\Request();
		//
		// $this->loader->add_filter( 'generate_rewrite_rules',	$request, 	'set_custom_rules'		,999);
		// $this->loader->add_filter( 'query_vars',				$request, 	'add_query_vars'		,999);
		// $this->loader->add_action( 'template_redirect',			$request,	'check_request'			,999);

		$service = new QMQUOTE\Front\Service();

		$this->loader->add_action( 'qmquote/form/prepare',					$service,	'get_json_data'		,999);
		$this->loader->add_action( 'wp_enqueue_scripts',					$service,	'set_local_data'	,1000000);
		$this->loader->add_filter( 'qmquote/service/data',					$service,	'get_data'			,999);
		$this->loader->add_filter( 'qmquote/service/detail',				$service,	'get_service_data'	,999,2);
		$this->loader->add_action( 'quoteme/rest/service/change',			$service, 	'update_service_data', 999,1);

		$calculate = new QMQUOTE\Front\Calculation();

		$this->loader->add_filter( 'qmquote/calculation/do'				,$calculate, 'call_calculate', 999,2);
		$this->loader->add_action( 'quoteme/rest/quotation/calculate'	,$calculate, 'do_calculate'	, 999);

		$quotation = new QMQUOTE\Front\Quotation();

		$this->loader->add_action( 'quoteme/rest/quotation/request'		,$quotation, 'save_request'	,999);
	}

	/**
	 * Run the loader to execute all of the hooks with WordPress.
	 *
	 * @since    1.0.0
	 */
	public function run() {
		$this->loader->run();
	}

	/**
	 * The name of the plugin used to uniquely identify it within the context of
	 * WordPress and to define internationalization functionality.
	 *
	 * @since     1.0.0
	 * @return    string    The name of the plugin.
	 */
	public function get_plugin_name() {
		return $this->plugin_name;
	}

	/**
	 * The reference to the class that orchestrates the hooks with the plugin.
	 *
	 * @since     1.0.0
	 * @return    Qmquote_Loader    Orchestrates the hooks of the plugin.
	 */
	public function get_loader() {
		return $this->loader;
	}

	/**
	 * Retrieve the version number of the plugin.
	 *
	 * @since     1.0.0
	 * @return    string    The version number of the plugin.
	 */
	public function get_version() {
		return $this->version;
	}

}
