<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://ridwan-arifandi.com
 * @since             1.0.1
 * @package           Qmquote
 *
 * @wordpress-plugin
 * Plugin Name:       QuoteMe
 * Plugin URI:        https://quotemewp.com
 * Description:       Display the basic pricing for your services and extras then provide quotations based on a user requests and requirements.
 * Version:           1.0.1
 * Author:            QuoteMe WP
 * Author URI:         https://quotemewp.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       qmquote
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

global $qmquote;

define('QUOTEME_APIURL'	,'https://app.quoteme.dev');
define('QUOTATION_URL'	,plugin_dir_url(__FILE__));
define('QUOTATION_PATH'	,plugin_dir_path(__FILE__));

require 'third-party/autoload.php';

/**
 * Currently plugin version.
 * Start at version 1.0.1 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'QMQUOTE_VERSION', '1.0.1' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-qmquote-activator.php
 */
function activate_qmquote() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-qmquote-activator.php';
	Qmquote_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-qmquote-deactivator.php
 */
function deactivate_qmquote() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-qmquote-deactivator.php';
	Qmquote_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_qmquote' );
register_deactivation_hook( __FILE__, 'deactivate_qmquote' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-qmquote.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.1
 */
function run_qmquote() {

	$plugin = new Qmquote();
	$plugin->run();

}

if(!function_exists('__debug')) :
    function __debug()
    {
        $bt     = debug_backtrace();
        $caller = array_shift($bt);
        ?><pre class='sejoli-debug'><?php
        print_r([
            "file"  => $caller["file"],
            "line"  => $caller["line"],
            "args"  => func_get_args()
        ]);
        ?></pre><?php
    }
endif;

if(!function_exists('sejoli_strpos_array')) :
/**
 * Extend strpos function to enable needle as array
 * @param  string   $haystack [description]
 * @param  mixed    $needle   [description]
 * @param  integer  $offset   [description]
 * @return boolean
 */
function sejoli_strpos_array($haystack,$needle,$offset = 0)
{
    if(!is_array($needle)) :
        $needle = array($needle);
    endif;

    foreach($needle as $query) :
        if(false !== strpos($haystack, $query, $offset)) :
             return true;
        endif;
    endforeach;

    return false;
}
endif;

run_qmquote();

/**
 * Plugin update checker
 */

require_once(QUOTATION_PATH.'library/plugin-update-checker/plugin-update-checker.php');

$update_checker = Puc_v4_Factory::buildUpdateChecker(
	'https://bitbucket.org/sitemaster-au/quotation-plugin/',
	__FILE__,
	'qmquote'
);

$update_checker->setAuthentication([
	'consumer_key'    => '9B77BdkTbdhdcJQNzX',
	'consumer_secret' => 'pALaQZCEyg5CdwtjET36ZpwSv7BbUz5a',
]);

$update_checker->setBranch('release');
