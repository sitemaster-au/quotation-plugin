=== Plugin Name ===
Contributors: Ridwan Arifandi
Donate link: https://ridwan-arifandi.com
Tags: form, quotation
Requires at least: 3.0.1
Tested up to: 3.4
Stable tag: 0.5.2
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Plugin to display quotation form. The data from quotation form will be sent to appquoteme.com

== Installation ==

This section describes how to install the plugin and get it working.

e.g.

1. Upload `quotation-quoteme.zip` to the `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress
3. Go to admin > QuoteMe Setup. Fill every fields to setup the plugin
4. Add shortcode [quoteme] in your desired page

== Frequently Asked Questions ==

= A question that someone might have =

An answer to that question.

= What about foo bar? =

Answer to foo bar dilemma.

== Screenshots ==

1. This screen shot description corresponds to screenshot-1.(png|jpg|jpeg|gif). Note that the screenshot is taken from
the /assets directory or the directory that contains the stable readme.txt (tags or trunk). Screenshots in the /assets
directory take precedence. For example, `/assets/screenshot-1.png` would win over `/tags/4.3/screenshot-1.png`
(or jpg, jpeg, gif).
2. This is the second screen shot

== Changelog ==

= 0.5.2
* Add update service button

= 0.5.1
* Update plugin info

= 0.5.0 =
* First release with automatic update
