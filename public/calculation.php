<?php
namespace QMQUOTE\Front;

class Calculation
{
    private $services;
    private $service;
    private $respond = [
        'days'          => 0,
        'page'          => '1 page',
        'compatibility' => 'Desktop',
        'turnaround'    => 'standard 7 day',
        'breakdown'     => [],
        'total'         => 0
    ];

    private $data = [
        'service'       => 0,
        'page'          => 1,
        'turnaround'    => 'standard',
        'compatibility' => [
            0 => [
                'value' => 'desktop'
            ]
        ],
        'name'        => '',
        'email'       => '',
        'instruction' => '',
        'file'        => '',
    ];

    private $compatibilities = [];

    /**
     * Construction
     */
    public function __construct()
    {

    }

    /**
     * Ca
     * @param  array    $compatibility [description]
     * @param  array    $service       [description]
     * @return integer                 [description]
     */
    private function calculateCostPerPage($delivery,$page_cost,$compatibilities)
    {
        $turnaround = $cost = 0;
        $extra      = intval($page_cost['express-pricing']);

        foreach($compatibilities as $comp) :

            if(isset($page_cost['pricing'][$comp])) :
                $pcost = intval($page_cost['pricing'][$comp]);
                if('express' === $delivery) :
                    $pcost += $pcost * ($extra/100);
                endif;

                $cost += $pcost;
            endif;
        endforeach;

        $this->respond['days'] += ('express' === $delivery) ? intval($page_cost['delivery']['express']) : $page_cost['delivery']['standard'];

        return $cost;
    }

    /**
     * Set selected service
     */
    protected function setService()
    {
        $service_id    = $this->data['service'];
        $this->service = (isset($this->services[$service_id])) ? $this->services[$service_id] : false;
    }

    /**
     * Set compatibilites
     */
    protected function setCompatibilities()
    {
        foreach((array) $this->data['compatibility'] as $comp) :
            $this->compatibilities[] = $comp['value'];
        endforeach;

        $this->respond['compatibility'] = ucwords(implode(', ',$this->compatibilities));
    }

    /**
     * Calculate page
     * @return
     */
    protected function calculatePage()
    {
        $inner_pages = [
            'page'  => 0,
            'cost'  => 0
        ];

        $page        = intval($this->data['page']);
        $page_number = 0;
        $delivery    = $this->data['turnaround'];

        for($i = 1;$i <= $page;$i++) :
            $key       = $i - 1;
            $page_cost = isset($this->service['pt'][$key]) ? $this->service['pt'][$key] : $page_cost;

            if(1 === $i) :

                $cost = $this->calculateCostPerPage($delivery,$page_cost,$this->compatibilities);

                $this->respond['breakdown'][] = [
                    'title' => 'Homepage',
                    'cost'  => '$'.round($cost,1)
                ];

                $this->respond['total'] += $cost;
            else :
                $inner_pages['page']++;
                $inner_pages['cost'] += round($this->calculateCostPerPage($delivery,$page_cost,$this->compatibilities),1);
            endif;
        endfor;

        if(0 !== intval($inner_pages['page'])) :
            $this->respond['breakdown'][] = [
                'title' => $inner_pages['page']. ' x Inner page(s)',
                'cost'  => '$'.round($inner_pages['cost'],1)
            ];

            $this->respond['total'] += $inner_pages['cost'];
        endif;

        $this->respond['page']       = sprintf(__('%d Page(s)','qmquote'),$page);
        $this->respond['turnaround'] = ucfirst($this->data['turnaround'].' '.$this->respond['days'].' day(s)');
    }

    /**
     * Calculate breakdown
     * @return
     */
    protected function calculateBreakdown()
    {
        $extras   = [];
        $delivery = $this->data['turnaround'];


        foreach((array) $this->data['extra'] as $extra) :
            $extras[]   = $extra['value'];
        endforeach;

        foreach((array) $this->service['additionals'] as $_add) :
            $express = intval($_add['express-pricing']);

            if(in_array($_add['title'],$extras)) :
                $price = $_add['price'];

                if('express' === $delivery) :
                    $price += $price * ($express / 100);
                endif;

                $this->respond['total'] += round($price,1);
                $this->respond['breakdown'][] = [
                    'title' => $_add['title'],
                    'cost'  => '$'.round($price,1)
                ];
            endif;
        endforeach;
    }

    /**
     * Calculate discount
     * @return void
     */
    protected function calculateDiscount()
    {
        if('false' !== $this->data['discount'] && false !== boolval($this->service['other']['discount']['active'])) :
            $amount = $this->service['other']['discount']['amount'];
            $this->respond['breakdown'][] = [
                'title' => 'Discount',
                'cost'  => '<span class="discount">-$'.round($amount,1).'</span>'
            ];

            $this->respond['total'] = $this->respond['total'] - round($amount,1);
        endif;
    }

    /**
     * Calculate all data
     * @return void
     */
    private function calculate()
    {
        $this->setService();
        $this->setCompatibilities();
        $this->calculatePage();
        $this->calculateBreakdown();
        $this->calculateDiscount();
    }

    /**
     * Do calculate of quotation
     * Hooked via action quoteme/rest/quotation/calculate, priority 999
     * @param  string $value [description]
     * @return [type]        [description]
     */
    public function do_calculate($value = '') {

        $this->respond['total'] = apply_filters('quoteme/calculate/get-total','',$_GET);

        wp_send_json($this->respond);
    }

    /**
     * Request to do calculate
     * Hooked via filter quoteme/calculate/request
     * @param  array $value
     * @return array
     */
    public function request_calculate($value = '',$args = array()) {

        $this->data     = wp_parse_args($args, $this->data);
        $this->services = apply_filters('qmquote/service/data',[]);

        $this->data['service'] = $this->data['service-id'];
        $this->data['page']    = $this->data['quantity'];

        $compatibility = $this->data['compatibility'];

        foreach((array) $compatibility as $comp) :
            $this->data['compatibility'][] = [
                'value' => $comp
            ];
        endforeach;

        $extra = $this->data['extra'];

        foreach((array) $extra as $ext) :
            $this->data['extra'][] = [
                'value' => $ext
            ];
        endforeach;

        $this->calculate();

        return $this->respond;
    }

    /**
     * Calculate hook action
     * Hooked via filter qmquote/calculation/do, priority 999
     * @param  mixed   $respond
     * @param  array   $post_data
     * @return array
     */
    public function call_calculate($respond = [],array $post_data)
    {
        $this->data     = wp_parse_args($post_data,$this->data);
        $this->services = apply_filters('qmquote/service/data',[]);
        $this->calculate();

        $this->respond['total'] = '$'.$this->respond['total'];

        return $this->respond;
    }
}
