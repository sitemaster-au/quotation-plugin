(function( $ ) {
	'use strict';
	var qmto,
		quoteme = {
		blockUI : {
			message : '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span>',
			css : {
				color : '#ffffff',
				border : 'none',
				backgroundColor : 'transparent'
			}
		},
		default : {
			page : 1,
			turnaround : 'standard',
			compatibility : 'desktop',
		},
		icon : {
			'other' : 'fa-television',
			'desktop' : 'fa-desktop',
			'tablet' : 'fa-tablet',
			'mobile' : 'fa-mobile',
		},
		data : {
			index : 0,
			service : '',
			services: [],
		},
		postData : {
			service : 0,
			page : 1,
			turnaround : '',
			compatibility : [],
			extra : [],
			name : '',
			email : '',
			instruction  : '',
			file : '',
			discount : '',
			day : 0
		},
		templates : {
			services : function() {
				var tmpl = $.templates('#quoteme-service'),
					html = '',
					checked = '';

				$.each(quoteme.data.services,function(i,service){
					checked = (0 === i) ? 'checked="checked"' : '';

					html += tmpl.render({
						checked : checked,
						id : service.id,
						key: service.id,
						color : service.color,
						image : service.image,
						title : service.title
					})
				})

				$('.quoteme-service-list').html(html);
			},
			compatibilities : function() {
				var tmpl = $.templates('#quoteme-compatibility'),
					html = '<ul class="checkbox2">',
					checked = '';
				$.each(quoteme.data.service.compatibilities,function(i,comp){
					checked = '';

					if(comp === quoteme.default.compatibility) {
						checked = 'checked="checked"';
					}

					html += tmpl.render({
						'icon' : quoteme.icon[comp],
						'name' : comp,
						'checked' : checked
					});
				});

				html += '</ul>';

				$('.quoteme-compatibility-list').html(html);
			},
			extra : function() {
				var tmpl = $.templates('#quoteme-extra'),
					html = '';

				html += tmpl.render(quoteme.data.service.extra);
				$('.quoteme-extra-list').html(html);
			},
			discount : function() {
				var tmpl = $.templates('#quoteme-discount'),
					html = '';

				if('active' == quoteme.data.service.discount.active) {
					console.log(quoteme.data.service.discount);
					html = tmpl.render(quoteme.data.service.discount);
				}

				$('.quoteme-discount-holder').html(html);
			},
			info : function() {
				var page = 1,
					html = '',
					total = 0;

				page = parseInt(quoteme.postData.page);

				if(1 < page) {
					$('.total-page-requested').html('1 Homepage + ' + (page - 1) + ' inner page(s) ');
				} else {
					$('.total-page-requested').html('1 Homepage');
				}

				if('standard' === quoteme.postData.turnaround) {
					$('.quoteme-turnaround-info .turnaround-type').html('standard turnaround');
				} else {
					$('.quoteme-turnaround-info .turnaround-type').html('express turnaround');
				}

				$('.quoteme-turnaround-info .total-pages').html(page + ' page(s)');

				var length = quoteme.postData.compatibility.length;

				$.each(quoteme.postData.compatibility,function(i,comp){

					if(0 === i) {
						html += comp.value;
					} else if( (length - 1) === i ) {
						html += ' and ' + comp.value;
					} else {
						html += ', ' + comp.value;
					}
				})

				$('.quoteme-compatibility-info').html('Fully compatible with ' + html + ' device(s)');
				html = '';

				$('.quoteme-turnaround-info .turnaround-time').html(quoteme.postData.day + ' business day(s)');

				$('input[name=service-id]').val(quoteme.data.service.id);

				var tmpl = $.templates('#quoteme-sidebar-header');

				$('.quoteme-sidebar-header').html(tmpl.render({
					logo : quoteme.data.service.logo,
					title : quoteme.data.service.title.substr(0,20)
				}));
			}
		},
		helpers : function() {

		},
		/***********************************************************************
		 * Event Change
		 ***********************************************************************/
		event : {
			pageChange : function() {
				$(document).on('click','.quoteme-page-size',function(){
					var target = $('.quoteme-field.quantity');

					if($(this).hasClass('plus')) {
						target.val(parseInt(target.val()) + 1);
					} else {
						var pg = parseInt(target.val()) - 1;
						if(0 === pg) {
							target.val(1);
						} else {
							target.val(pg);
						}
					}

					target.trigger('change');
				});
			},
			compatibiltyChange : function() {
				$(document).on('click','.quoteme-compatibility-list li',function(){
					var checkbox = $(this).find('input[type=checkbox]');
					checkbox.prop("checked",!checkbox.prop("checked"));
					checkbox.trigger('change');
				});
			},
			turnaroundChange : function() {
				$(document).on('click','.quoteme-turnaround-list li',function(){
					$(this).parent().find('input[type=radio]').attr('checked',false).trigger('change');
					$(this).find('input[type=radio]').attr('checked',true);
				});
			},
			fieldChange : function() {
				$(document).on('change','.quoteme-field',function(){
					clearTimeout(qmto);
					qmto = setTimeout(function(){
						console.info('process to calculate');
						quoteme.checkForm();
						quoteme.calculateTurnaround();
						quoteme.calculateCost();
						quoteme.templates.info();
					},500)
				});
			},
			serviceChange : function() {
				$(document).on('click','.quoteme-service',function(){
					quoteme.data.index = $(this).val();
					quoteme.setService(qmservice[quoteme.data.index]);

					quoteme.templates.compatibilities();
					quoteme.templates.extra();
					quoteme.templates.discount();
					quoteme.checkForm();
					quoteme.calculateTurnaround();
					quoteme.calculateCost();
					quoteme.templates.info();
				});
			},
			submit : function() {
				$(document).on('submit','#qmquotation-form',function(){

					$.ajax({
						url 	: qmrequest.request,
						type 	: 'POST',
						data	: new FormData(this),
						processData: false,
						contentType : false,
						cache : false,
						beforeSend : function() {
							$('#qmquotation-form').block(quoteme.blockUI);
						},
						success : function(response) {
							$('#qmquotation-form').unblock();
							$('.container > .grid').block({
								message : '<div class="blockUI-message">' + response.messages[0] + '</div>',
								css : {
									width : '480px',
									color : '#ffffff',
									border : 'none',
									backgroundColor : 'transparent'
								}
							});
							setTimeout(function(){
								location.reload();
							},1000);
						}
					})
					return false;
				});
			}
		},
		init : function() {
			var i = 0;
			$.each(qmservice,function(index,service){
				quoteme.data.services.push({
					'id' 	: service.ID,
					'color' : service.color,
					'image' : service.image,
					'title' : service.title
				});

				if(0 === i) { quoteme.setService(service);}

				i++;
			});

			quoteme.templates.services();
			quoteme.templates.compatibilities();
			quoteme.templates.extra();
			quoteme.templates.discount();
			quoteme.checkForm();
			quoteme.calculateTurnaround();
			quoteme.calculateCost();
			quoteme.templates.info();

			quoteme.event.serviceChange();
			quoteme.event.compatibiltyChange();
			quoteme.event.turnaroundChange();
			quoteme.event.fieldChange();
			quoteme.event.pageChange();
			quoteme.event.submit();
		},
		setService : function(service) {
			quoteme.data.service = {
				id : service.ID,
				logo : service.image,
				title : service.title,
				discount : service.other.discount,
				compatibilities : service.compatibility,
				extra : service.additionals,
				pt : service.pt
			}
		},
		checkForm : function(){
			var name = '';

			quoteme.postData.compatibility = [];
			quoteme.postData.extra = [];

			quoteme.postData.service = $('.quoteme-service:checked').val();


			$('body').find('.quoteme-field').each(function(i,el){

				name = ('checkbox' === $(el).attr('type')) ? $(el).data('name') : $(el).attr('name');

				if('quantity' === name) {
					quoteme.postData.page = $(el).val();
				} else if('turnaround' === name && $(el).is(':checked')) {
					quoteme.postData.turnaround = $(el).val();
				} else if('compatibility' === name && $(el).is(':checked')) {
					quoteme.postData.compatibility.push({
						value : $(el).val()
					});
				} else if('extra' === name && $(el).is(':checked')) {
					quoteme.postData.extra.push({
						value : $(el).val()
					});
				} else if('discount' === name) {
					quoteme.postData.discount = $(el).is(':checked');
				}
			});
		},
		calculateTurnaround : function() {
			var page = 1,
				type = 'standard',
				pt = '',
				key = 0,
				day = 0,
				processDay = 0;

			page = parseInt(quoteme.postData.page);
			type = quoteme.postData.turnaround;
			day = 0;

			for(var i = 1;i <= page;i++) {
				key = i - 1;
				if(typeof quoteme.data.service.pt[key] !== 'undefined') {
					processDay = parseInt(quoteme.data.service.pt[key].delivery[type])
				}

				day += processDay;
			}

			quoteme.postData.day = day;
		},
		calculateCost: function() {
			$.ajax({
				url : qmrequest.calculate,
				data : quoteme.postData,
				dataType : 'json',
				beforeSend : function() {
					$('#quoteme-breakdown-sidebar').block(quoteme.blockUI);
				},
				success : function(response){
					var html = '',
						tmpl = $.templates('#quoteme-breakdown');

					$('.quoteme-total-page').html(response.page);
					$('.quoteme-compatibility-detail').html(response.compatibility);
					$('.quoteme-turnaround-time').html(response.turnaround);
					$('.quoteme-price-breakdown').html(tmpl.render(response.breakdown));
					$('.quoteme-total-fee').html(response.total);

					$('#quoteme-breakdown-sidebar').unblock();
				}
			})
		},
		getService: function(service_id) {
			console.info(qmservice);
		}
	};

	$(document).ready(function(){
		quoteme.init();
	});

})( jQuery );
