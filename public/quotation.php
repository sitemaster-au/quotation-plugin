<?php
namespace QMQUOTE\Front;

class Quotation
{
    protected $post    = [
        'qmquote'       => '',
        'service-id'    => 0,
        'client-email'  => '',
        'client-name'   => '',
        'compatibility' => [],
        'discount'      => false,
        'extra'         => [],
        'instruction'   => '',
        'quantity'      => 1,
        'turnaround'    => 'standard',
        'file'          => '',
    ];

    protected $file     = [];
    protected $respond  = true;
    protected $messages = [];

    /**
     * Construction
     */
    public function __construct()
    {

    }

    /**
     * Sanitize data
     * @return void
     */
    protected function sanitize_data()
    {
        $this->post['client-email'] = sanitize_email($this->post['client-email']);
        $this->post['discount']     = boolval(sanitize_email($this->post['discount']));
        $this->post['quantity']     = absint($this->post['quantity']);
        $this->post['service-id']   = absint($this->post['service-id']);
        $this->post['instruction']  = esc_html($this->post['instruction']);
    }

    /**
     * Validate client data
     * @return void
     */
    protected function validate_client()
    {
        if(!wp_verify_nonce($this->post['qmnonce'],'qmquote-request')) :
            $this->respond      = false;
            $this->messages[]   = __('Something wrong with the form process. You need to ask the administrator','qmquote');
        endif;

        if(0 === $this->post['service-id']) :
            $this->respond      = false;
            $this->messages[]   = __('Service is not valid or not selected','qmquote');
        endif;

        if(empty($this->post['client-name'])) :
            $this->respond = false;
            $this->messages[] = __('Your name is empty','qmquote');
        endif;

        if(!is_email($this->post['client-email'])) :
            $this->respond = false;
            $this->messages[] = __('Your email address is not valid','qmquote');
        endif;
    }

    /**
     * Validate quotation
     * @return void
     */
    protected function validate_quotation()
    {
        if(!is_array($this->post['compatibility']) || 0 === count($this->post['compatibility'])) :
            $this->respond = false;
            $this->messages[] = __('Compatibility is not set','qmquote');
        endif;

        if(!in_array($this->post['turnaround'],['standard','express'])) :
            $this->respond = false;
            $this->messages[] = __('Turnaround time is not valid','qmquote');
        endif;

        if(0 === $this->post['quantity']) :
            $this->respond = false;
            $this->messages[] = __('Number of pages\' value is not valid','qmquote');
        endif;

        if(empty($this->post['instruction'])) :
            $this->respond = false;
            $this->messages[] = __('Instruction is empty','qmquote');
        endif;
    }

    /**
     * Validate file
     * @return void
     */
    protected function validate_file()
    {
        if(isset($this->file['file'])) :

            $file = $this->file['file'];

            if('application/zip' === $file['type'] && 0 === intval($file['error'])) :

            elseif(4 === intval($file['error'])) :
                $this->file['file'] = false;
            else :
                $this->respond      = false;
                $this->messages[]   = __('The file you uploaded is not valid zip file','qmquote');
            endif;
        endif;
    }

    /**
     * Upload attachment file
     * @return void
     */
    protected function upload_file()
    {
        if(false === $this->file['file']) :
            return;
        endif;

        $maxsize = wp_max_upload_size();

        // add zip  to available upload file type
        add_filter('upload_mimes',function($existing_mimes = array()){
            $existing_mimes['zip'] = 'application/zip';
            return $existing_mimes;
        });

        if($this->file['file']['size'] > $maxsize) :
            $this->respond      = false;
            $this->messages[]   = sprintf(__('Max upload file size is %s','qmquote'),($maxsize / (1024 * 1024)).'MB');
            return;
        endif;

        $file   = $this->file['file'];
        $upload = wp_upload_bits($file['name'],null,file_get_contents($file['tmp_name']));

        if(isset($upload['error']) && !empty($upload['error'])) :
            $this->respond      = false;
            $this->messages[]   = $upload['error'];
            return;
        endif;

        $this->post['file'] = $upload;
    }

    /**
     * Do calculation
     * @return void
     */
    protected function do_calculation()
    {
        $args = $this->post;

        unset($args['compatibility'],$args['extra']);

        $args['service']     = $this->post['service-id'];
        $args['page']        = $this->post['quantity'];

        foreach($this->post['compatibility'] as $comp) :
            $args['compatibility'][]['value'] = $comp;
        endforeach;

        foreach($this->post['extra'] as $extra) :
            $args['extra'][]['value'] = $extra;
        endforeach;

        $calculation = apply_filters('qmquote/calculation/do',false,$args);

        if(isset($calculation['total'])) :
            $this->post['calculation'] = $calculation['total'];
        endif;
    }

    /**
     * Do the save quotation
     * @return void
     */
    protected function do_save()
    {
        if(true === $this->respond) :

            $service = apply_filters('qmquote/service/detail',[],$this->post['service-id']);

            if(false !== $service) :

                $this->upload_file();

                if(true === $this->respond) :

                    $this->do_calculation();
                    
                    do_action('qmquote/request/create-quotation',$this->post,$this->file);

                    $args = [
                        'post_title'    => '',
                        'post_type'     => 'quotation',
                        'post_status'   => 'publish',
                        'post_content'  => $this->post['instruction']
                    ];

                    $args['ID']         = $post_id = wp_insert_post($args);
                    $args['post_title'] = sprintf(__('Quotation #%d for service %s','qmquote'),$post_id,$service['title']);

                    wp_update_post($args);

                    update_post_meta($post_id,'qmquote',$this->post);

                    include_once( ABSPATH . 'wp-admin/includes/image.php' );

                    $attach_id   = wp_insert_attachment([
                                        'post_title' => $this->file['file']['name'],
                                        'guid'       => $this->post['file']['url'],
                                        'post_status'=> 'inherit',
                                        'post_type'  => 'attachment'
                                    ], $this->post['file']['file'], $post_id );

                    $attach_data = wp_generate_attachment_metadata( $attach_id, $this->post['file']['file']);
                    $update      = wp_update_attachment_metadata( $attach_id, $attach_data );

                    // Send request
                    $this->messages[]   = sprintf(__('Quotation created successfully','qmquote'));

                endif;

            else :
                $this->respond      = false;
                $this->messages[]   = __('Service is not valid ID. Contact administrator','qmquote');
            endif;
        endif;
    }

    /**
     * Save quotation data
     * Hooked via action quoteme/rest/quotation/request, priority 999
     * @param  string $value [description]
     * @return [type]        [description]
     */
    public function save_request($value = '')
    {
        global $qmquote;

        $this->post            = wp_parse_args($_POST,$this->post);
        $this->file            = $_FILES;

        $this->sanitize_data();
        $this->validate_client();
        $this->validate_quotation();
        $this->validate_file();
        $this->do_save();

        wp_send_json([
            'respond'   => apply_filters('qmquote/connection/respond',$this->respond),
            'messages'  => apply_filters('qmquote/connection/messages',$this->messages),
            'data'      => isset($qmquote['rest']['data']) ? $qmquote['rest']['data'] : NULL
        ]);
    }
}
