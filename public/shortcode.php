<?php
namespace QMQUOTE\Front;

class Shortcode
{
    /**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

    private $is_form_page = false;
	private $is_shortcode_active = false;

    /**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

        add_shortcode('quoteme' ,[$this,'display_shortcode']);
    }

    /**
     * Check if currenct page has quoteme shortcode
     * Hooked via action template_redirect, priority 999
     * @return void
     */
    public function check_shortcode()
    {
        global $post;

        if(has_shortcode($post->post_content,'quoteme')) :
            $this->is_shortcode_active = true;
        endif;
    }

    /**
     * Render shortcode contents
     * @param  array    $atts
     * @param  string   $content
     * @return string
     */
    public function display_shortcode($atts,$content)
    {
        ob_start();
		?>
		<iframe class='quotation-quoteme-iframe' src="<?php echo home_url('quoteme/form/display');?>" width="100%" height="" onload="quoteme_quotation.iframe_set(this)"></iframe>
		<?php
        $content = ob_get_contents();
        ob_end_clean();

        return $content;
    }

    /**
     * Check if the shortcode exists within content
     * Hooked via qmquote/shortcode/status, priority 999
     * @return boolean
     */
    public function does_shortcode_exist()
    {
        return $this->is_form_page;
    }

    /**
     * Set CSS and JS files
     * Hooked via wp_enqueue_scripts, priority 999
     * @return void
     */
    public function set_css_js_scripts()
    {
		/**
		 * Check if form page active then will enqueue needed style and script
		 */
        if(false !== $this->is_form_page) :
            wp_register_style   ('font-awesome' ,'https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css');
            wp_enqueue_style    ('qmquote-form' ,QUOTATION_URL.'/public/css/form.css',['font-awesome'],$this->version);

            wp_register_script  ('jsrender'		   ,'https://cdnjs.cloudflare.com/ajax/libs/jsrender/1.0.1/jsrender.min.js',['jquery'],'1.0.1',true);
            wp_register_script  ('jquery-blockui'  ,'https://cdnjs.cloudflare.com/ajax/libs/jquery.blockUI/2.70/jquery.blockUI.min.js',['jquery'],'2.70',true);
            wp_enqueue_script   ('qmquote-form'    ,QUOTATION_URL.'/public/js/qmquote-public.js',['jquery','jsrender','jquery-blockui'],$this->version,true);
            wp_localize_script  ('qmquote-form','qmrequest',[
                'calculate'         => home_url('quoteme/quotation/calculate'),
                'request'           => home_url('quoteme/quotation/request'),
                'changeService'     => home_url('quoteme/service/change')
            ]);
        endif;

		/**
		 * Check if shortcode active then will enqueue needed style and script
		 */
		if(false !== $this->is_shortcode_active) :
			wp_enqueue_style 	('qmquote-display'	, QUOTATION_URL . '/public/css/qmquote-display.css'	, [], $this->version);
			wp_enqueue_script 	('qmquote-display'	, QUOTATION_URL . '/public/js/qmquote-display.js'	, ['jquery'], $this->version,true);
		endif;
    }

	/**
	 * Display quotation form
	 * Hooked via action quoteme/rest/form/display, priority 999
	 * @return	void
	 */
	public function display_form() {

		$this->is_form_page = true;

		show_admin_bar(false);

		do_action('qmquote/form/prepare');
		$services = apply_filters('qmquote/service/data',[]);

		require plugin_dir_path(__FILE__).'partials/form.php';
	}
}
