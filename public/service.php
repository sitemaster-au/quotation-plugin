<?php
namespace QMQUOTE\Front;

class Service
{
    private $json_file;
    protected $service = NULL;

    /**
     * Construction
     */
    public function __construct()
    {
        $this->json_file = QUOTATION_PATH.'/json/data.json';
    }

    /**
     * Get service JSON data
     * Hooked via action qmquote/form/prepare, priority 999
     * @return void
     */
    public function get_json_data()
    {
        if(file_exists($this->json_file) && NULL === $this->service) :
            $thefile       = fopen($this->json_file,'r');
            $content       = fread($thefile,filesize($this->json_file));
            $this->service = json_decode($content,true);
            fclose($thefile);
        endif;
    }

    /**
     * Set local data into javascript variabel
     * Hooked via wp_enqueue_scripts, priority 999
     * @return void
     */
    public function set_local_data()
    {
        if(NULL !== $this->service) :
            wp_localize_script('qmquote-form','qmservice',$this->service);
        endif;
    }

    /**
     * Get service data
     * Hooked via filter qmquote/service/data, priority 999
     * @return void
     */
    public function get_data($data = array())
    {
        if(NULL === $this->service) :
            do_action('qmquote/form/prepare');
        endif;

        return $this->service;
    }

    /**
     * Get single service data
     * Hooked via filter qmquote/service/detail, priority 999
     * @param  array    $service
     * @param  integer  $service_id
     * @return array|boolean
     */
    public function get_service_data($service = array(),$service_id)
    {
        if(NULL === $this->service) :
            $this->get_json_data();
        endif;

        if(isset($this->service[$service_id])) :
            return $this->service[$service_id];
        else :
            false;
        endif;

        return $service;
    }

    /**
     * Update service data
     * Hooked via action quoteme/rest/service/update
     * @return json
     */
    public function update_service_data()
    {

    }
}
