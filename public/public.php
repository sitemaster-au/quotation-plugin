<?php
namespace QMQUOTE;

/**
 * The public-facing functionality of the plugin.
 *
 * @link       https://ridwan-arifandi.com
 * @since      1.0.0
 *
 * @package    Qmquote
 * @subpackage Qmquote/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Qmquote
 * @subpackage Qmquote/public
 * @author     Ridwan Arifandi <orangerdigiart@gmail.com>
 */
class Front {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * List of CSS that will be enabled in custom page
	 * @var array
	 */
	private $css_enabled = [
		'key' => [],
		'src' => []
	];

	/**
	 * List of CSS that will be disabled in custom page
	 * @var array
	 */
	private $css_disabled = [
		'key' => [],
		'src' => []
	];

	/**
	 * List of JS that will be enabled in custom page
	 * @var array
	 */
	private $js_enabled = [
		'key' => [],
		'src' => []
	];

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;
		$this->enable_css_and_js();

	}

	/**
	 * Give permission to selected CSS and JS
	 * @return void
	 */
	private function enable_css_and_js()
	{
		$this->css_enabled = [
			'src' => [
				'wp-includes',
				'wp-admin'
			],
			'key' => [
				'wc-',
				'query-monitor'
			]
		];

		$this->js_enabled = [
			'src' => [
				'wp-includes',
				'wp-admin'
			],
			'key' => [
				'wc-',
				'query-monitor'
			]
		];

		$this->css_disabled = [
			'src' => [],
			'key' => [
				'woocommerce-general',
				'woocommerce-layout',
				'woocommerce-smallscreen'
			]
		];
	}

	/**
	 * Remove unneeded CSS and JS scripts
	 * @return void
	 */
	private function remove_unneeded_scripts()
	{
		global $wp_styles,$wp_scripts;
		$styles = $scripts = [];

        // REMOVE ALL UNWANTED STYLES AND SCRIPTS
        foreach( (array) $wp_styles->registered as $key => $object) :
            if(!(
					false !== sejoli_strpos_array($object->src,$this->css_enabled['src']) ||
					false !== sejoli_strpos_array($key,$this->css_enabled['key']) ||
                    '' === $object->src ||
                    1  === $object->src )
            ) :
                wp_dequeue_style($key);
			else :
				$styles[]	= $key;
            endif;
        endforeach;

        foreach((array) $wp_scripts->registered as $key => $object) :
            if(!(
				false !== sejoli_strpos_array($object->src,$this->js_enabled['src']) ||
				false !== sejoli_strpos_array($key,$this->js_enabled['key']) ||
				'' === $object->src ||
				1  === $object->src )
            ) :
                wp_dequeue_script($key);
			else :
				$scripts[]	= $key;
            endif;
        endforeach;
	}

	/**
	 * Register need CSS and JS
	 * @return void
	 */
	private function register_needed_scripts()
	{
		// // Register CSS
		// wp_register_style	('spectre-ui'					,'https://unpkg.com/spectre.css/dist/spectre.min.css');
		// wp_register_style	($this->plugin_name. '-main'	,plugin_dir_url(__FILE__) . 'css/main.css'	,['spectre-ui'] ,$this->version,'all');
		//
		// wp_enqueue_style	($this->plugin_name. '-main');
		//
		// // Register JS
		//
		//
		// wp_register_script	($this->plugin_name. '-main'	,plugin_dir_url(__FILE__) . 'js/main.js'	,['jquery','jsrender']	,$this->version,	true);
		//
		// wp_enqueue_script 	($this->plugin_name. '-main');
	}

	/**
	 * Setup CSS and JS scripts
	 * Hooked via action wp_enqueue_scripts, priority 999999
	 * @return void
	 */
	public function set_css_js_scripts()
	{
		$active = apply_filters('qmquote/shortcode/status',false);

		if(false !== $active) :
			$this->remove_unneeded_scripts();
			$this->register_needed_scripts();
		endif;
		//exit;

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {


		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/qmquote-public.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/qmquote-public.js', array( 'jquery' ), $this->version, false );

	}

}
