<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Nineteen
 * @since 1.0.0
 */
?><!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<link rel="profile" href="https://gmpg.org/xfn/11" />
	<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="<?php echo QUOTATION_URL; ?>/public/css/form.css?ver=<?php echo QMQUOTE_VERSION; ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo QUOTATION_URL; ?>/public/css/custom-form.css?ver=<?php echo QMQUOTE_VERSION; ?>">
	<?php wp_head(); ?>
</head>

<body <?php body_class('quotation-quoteme'); ?>>
	<div class="container">
		<header class="group">
			<div class="group center">
		      	<h1 class="logo">Get Your Quote</h1>
			  	<span class="tagline">No upfront payment is required at this stage. We will get back to you in the fastest possible way.</span>
	      	</div>
		    <div class="group item-list center quoteme-service-list">

	      	</div>
	    </header>

	    <section class="grid">
	    	<div class="col-2-3">
				<form id='qmquotation-form' method='POST' action="">
		    		<div class="group center line-padding"><h3>Number of Pages</h3></div>
		    		<div class="group center">
			    		<div class="number-input">
						  <button type='button' class='quoteme-page-size minus'></button>
						  <input class="quoteme-field quantity" min="1" name="quantity" value="1" min="1" type="number" />
						  <button type='button' class="quoteme-page-size plus"></button>
						</div>
					</div>
					<div class="group center">
						<small class='total-page-requested'>1 Homepage + 2 inner pages</small>
					</div>

		    		<div class="group center line-padding"><h3>Turnaround Time</h3></div>
		    		<div class="group center quoteme-turnaround-list">
		    			<ul class="radio2">
							<li>
							    <input type="radio" id="turnaround-standard" name="turnaround" value='standard'  checked="checked" class="quoteme-field radio" />
							    <label for="turnaround1">STANDARD TURNAROUND</label>
							</li>
							<li>
							    <input type="radio" id="turnaround-express" name="turnaround" value='express' class="quoteme-field radio" />
							    <label for="turnaround2">EXPRESS TURNAROUND</label>
							</li>
						</ul>
		    		</div>
		    		<div class="group center quoteme-turnaround-info">
						<small>Usually
							<strong class='turnaround-type'>standard turnaround</strong> takes
							<strong class='turnaround-time'>6 business days</strong> for
							<strong class='total-pages'>3 pages</strong>.
						</small>
					</div>
		    		<div class="group center line-padding">
						<h3>Select Compatibility</h3>
					</div>
		    		<div class="group center compatibility quoteme-compatibility-list">

		    		</div>
		    		<div class="group center">
						<small class='quoteme-compatibility-info'>Fully compatible with desktop, mobile browser and devices.</small>
					</div>

		    		<div class="group center line-padding line-padding-bottom"><h3>Extras</h3></div>
		    		<div class="group quoteme-extra-list">

		    		</div>

		    		<div class="group center line-padding line-padding-bottom">
		    			<h3>Enter Your Order Detail</h3>
		    			<p>Please provide us with as much detailed information as possible</p>
		    		</div>
		    		<div class="group center form-fields">
		    			<div class="col-1-2">
		    				<input type="text" size="30" name="client-name" placeholder="Your name" required />
		    			</div><!--
		    		 --><div class="col-1-2">
		    				<input type="email" size="30" name="client-email" placeholder="Email Address" required />
		    			</div>

		    			<div class="col-full" style="padding-top: 15px;">
		    				<textarea name='instruction' rows="6" placeholder="Instruction" required></textarea>
		    			</div>
		    		</div>

		    		<div class="group center line-padding-top line-padding-bottom">
						<p>We keep all information confidential</p>
					</div>
		    		<div class="group center form-fields">
		    			<div class="col-full">
		    				<input type="file" size="40" name="file" />
		    			</div>
		    		</div>
		    		<div class="group center line-padding">
		    			<p>
							We take PSD, Sketch, PNG, AI, PDF, EPS, JPEG, and ZIP Please include custom fonts if necessary.<br />
							Single file only. Max size is 512MB
						</p>
		    		</div>

		    		<div class="group center form-fields quoteme-discount-holder">
		    			<div class="col-full">
		    				<input type="checkbox" name="discount" value='1' class='quoteme-field' data-name='discount' />
							<label>
								Yes you can use this project in your sample section <span clas='red'>($40 discount)</span>
							</label>
		    			</div>
		    		</div>

		    		<div class="group center line-padding-top">
		    			<div class="col-full">
		    				<button class="btn-alt">ORDER NOW</button>
		    			</div>
		    		</div>
		    		<div class="group line-padding-top center">
						<h4>No upfront payment</h4>
						<p>No upfront payment is required at this stage.</p>
					</div>
					<div class="group line-padding-top center">
						<h4>Our Policies</h4>
						<p>By submitting your request, you agree with our policies</p>
					</div>
					<div class="group line-padding-top center">
						<h4>Turnaround & Price Term</h4>
						<p>
							Be aware that timeline and price may vary depending on project complexity.<br />
							The delivery date is provided after detailed project investigation.
						</p>
					</div>
					<input type="hidden" name="service-id" value="" />
					<?php wp_nonce_field('qmquote-request','qmnonce'); ?>
				</form>
	    	</div><!-- SIDEBAR
			--><div id='quoteme-breakdown-sidebar' class="col-1-3 sidebar">
	    		<div class="group center title-side" >
	    			<div class="group side-item">
						<div id="wp" class="tabcontent quoteme-sidebar-header">

						</div>
					</div>
				</div>

				<div class="content line-padding-8">
					<div class="group border-bottom side-item">
						<h4>NUMBER OF PAGES</h4>
						<p class='quoteme-total-page'>3 Pages</p>
					</div>
					<div class="group border-bottom side-item">
						<h4>COMPATIBILITY</h4>
						<p class='quoteme-compatibility-detail'>Desktop, Mobile</p>
					</div>
					<div class="group side-item">
						<h4>TURNAROUND</h4>
						<p class='quoteme-turnaround-time'>Standard 7 days</p>
					</div>
				</div>
				<div class="group sub-title-side" >
					<div class="group side-item">
						<h4>PRICE BREAKDOWN</h4>
						<div class="quoteme-price-breakdown">

						</div>
					</div>
				</div>
				<div class="group foot-title-side" >
					<div class="group side-item">
						<div class="total">TOTAL <span class="right quoteme-total-fee">$440</span></div>
						<small>*no upfront payment required.</small>
					</div>
				</div>

	    	</div>
	    </section>
	</div>

	<script id='quoteme-sidebar-header' type="text/x-jsrender">
	<img src="{{:logo}}" width="50px;">
	<h3>{{:title}}</h3>
	</script>
	<script id='quoteme-service' type="text/x-jsrender">
	<label class="item">
		<input type="radio" name="service" value="{{:id}}" id="service-{{:key}}" {{:checked}} class='quoteme-service'>
	  	<img src="{{:image}}" style="width: 100px; padding: 15px;">
	  	<p>{{:title}}</p>
	</label>
	</script>
	<script id='quoteme-compatibility' type="text/x-jsrender">
	<li>
		<input type="checkbox" data-name='compatibility' id="compatibility-{{:name}}" name="compatibility[]" value="{{:name}}" class="quoteme-field" {{:checked}}>
		<label for="other">
			<i class="fa {{:icon}}" aria-hidden="true"></i>
			<small>{{:name}}</small>
		</label>
	</li>
	</script>
	<script id='quoteme-extra' type="text/x-jsrender">
	<div class="col-1-2 extras">
		<label class="extras">{{:title}} (+${{:price}})
		  <input type="checkbox" data-name='extra' name='extra[]' value='{{:title}}' class='quoteme-field' />
		  <span class="checkmark"></span>
		</label>
	</div>
	</script>
	<script id='quoteme-breakdown' type="text/x-jsrender">
	<div class="item border-bottom">{{:title}}<span class="right">{{:cost}}</span></div>
	</script>

	<script id='quoteme-discount' type="text/x-jsrender">
	<div class="col-full">
		<input type="checkbox" name="discount" value='1' class='quoteme-field' data-name='discount' />
		<label>
			{{:text}} <span clas='red'>({{:amount}}$ {{:highlight}})</span>
		</label>
	</div>
	</script>
    <?php wp_footer(); ?>
</body>
</html>
