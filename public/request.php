<?php
namespace QMQUOTE\Front;

class Request
{
    private $url;
    private $demo = true;
    /**
     * Construction
     */
    public function __construct()
    {
        $this->url  = home_url('qmquote');
    }

    /*
    * Register custom rules
    * Hooked via filter generate_rewrite_rules, priority 999
    * @param Object $wp_rewrite
    */
    public function set_custom_rules($wp_rewrite)
    {
        $wp_rewrite->rules['qmquote/([^/]+)/([^/]+)?$']         = 'index.php?qmquote=$matches[1]&action=$matches[2]';
        $wp_rewrite->rules['qmquote/([^/]+)/([^/]+)/([^/]+)?$'] = 'index.php?qmquote=$matches[1]&action=$matches[2]&value=$matches[3]';

        return $wp_rewrite;
    }

    /**
     * Add custom query vars
     * Hooked via filter query_vars, priority 999
     * @param array $vars [description]
     */
    public function add_query_vars($vars)
    {
        $vars[] = 'qmquote';
        $vars[] = 'action';
        $vars[] = 'value';

        return $vars;
    }

    /**
     * Check endpoint request
     * Hooked via action template_redirect, priority 999
     * @return void
     */
    public function check_request()
    {
        global $wp_query;

        if('' !== get_query_var('qmquote') && '' !== get_query_var('action')) :

            $controller = get_query_var('qmquote');
            $action     = get_query_var('action');
            $value      = get_query_var('value');

            $hook       = 'qmquote/'.$controller.'/'.$action;
            echo 'tet ['.$hook.']';
            __debug($hook);
            // do custom action
            do_action($hook,$value);

            exit;
        endif;
    }
}
