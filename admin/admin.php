<?php
namespace QMQUOTE;

use Carbon_Fields\Container;
use Carbon_Fields\Field;

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       https://ridwan-arifandi.com
 * @since      1.0.0
 *
 * @package    Qmquote
 * @subpackage Qmquote/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Qmquote
 * @subpackage Qmquote/admin
 * @author     Ridwan Arifandi <orangerdigiart@gmail.com>
 */
class Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		 wp_register_style	('fontawesome-v4',	'https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css');
		 wp_enqueue_style	($this->plugin_name, 				plugin_dir_url( __FILE__ ) . 'css/qmquote-admin.css', array(), $this->version, 'all' );
		 wp_enqueue_style 	($this->plugin_name.'-quotation',	plugin_dir_url( __FILE__ ) . 'css/qmquote-quotation.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {
		wp_register_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/qmquote-admin.js', array( 'jquery' ), $this->version, true );
		wp_localize_script( $this->plugin_name, 'qmquote_var',[
			'ajaxurl'	=> [
				'update_service'	=> add_query_arg(['action' => 'request_update_service'],admin_url('admin-ajax.php'))
			]
		]);
	}

	/**
	 * Load carbonfields
	 * Hooked via action after_setup_theme, priority 999
	 * @return void
	 */
	public function load_carbon_fields()
	{
		\Carbon_Fields\Carbon_Fields::boot();
	}

	/**
	 * Set plugin options
	 * Hooked via action carbon_fields_register_fields, priority 999
	 */
	public function set_plugin_options()
	{
		ob_start();
		require QUOTATION_PATH.'admin/partials/help/about.php';
		$quoteme_about_html = ob_get_contents();
		ob_end_clean();

		ob_start();
		require QUOTATION_PATH.'admin/partials/help/howto.php';
		$quoteme_howto_html = ob_get_contents();
		ob_end_clean();

		ob_start();
		require QUOTATION_PATH.'admin/partials/help/upgrade.php';
		$quoteme_upgrade_html = ob_get_contents();
		ob_end_clean();

		Container::make('theme_options'	,'QuoteMe Setup')
			->add_tab(__('About QuoteMe','quoteme'),[
				Field::make('html','quoteme_about_html',__('About QuoteMe','quoteme'))
					->set_html($quoteme_about_html)
			])
			->add_tab(__('API','quoteme'),[
				Field::make('text'	,'quoteme_account'	,'Your Quoteme Username')
					->set_required(true),
				Field::make('text'	,'quoteme_password'	,'Your Quoteme Password')
					->set_attribute('type','password')
					->set_required(true),
				Field::make('text'	,'quoteme_api_key','API Key')
					->set_required(true)
			])
			->add_tab(__('How To','quoteme'),[
				Field::make('html','quoteme_howto_html',__('How To','quoteme'))
					->set_html($quoteme_howto_html)
			])
			->add_tab(__('Upgrade','quoteme'),[
				Field::make('html','quoteme_upgrade_html',__('Upgrade','quoteme'))
					->set_html($quoteme_upgrade_html)
			]);
	}

	/**
	 * Add plugin info
	 * Hooked via filter plugin_row_meta, priority 10
	 * @param 	array  $links [description]
	 * @param 	[type] $file  [description]
	 * @return	array
	 */
	public function add_plugin_info(array $links,$file)
	{
		list($plugin_folder,$plugin_file) = explode('/',$file);

		if('qmquote.php' === $plugin_file) :
			$row_meta     = [];
			$setting_link = add_query_arg([
								'page' => 'crb_carbon_fields_container_quoteme_setup.php'
							],admin_url('admin.php'));
			$row_meta[]   = '<a href="'.$setting_link.'">Setting</a>';
			$row_meta[]   = '<a href="#">API</a>';
			$row_meta[]   = '<a href="https://quotemewp.com">Create QuoteMe account</a>';
			$row_meta[]   = '<a href="#">How to</a>';

			return array_merge($links,$row_meta);
		endif;
		return $links;
	}
}
