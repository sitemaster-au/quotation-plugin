<?php
    $calc = $quotation['calculation'];
    $file = $quotation['file'];
?>
<table class='form-table'>
    <tr>
        <th scope="row"><?php _e('Client','qmquote'); ?></th>
        <td><?= $quotation['client-name']; ?>, &lt;<?= $quotation['client-email']; ?>&gt;</td>
    </tr>
    <tr>
        <th scope="row"><?php _e('Instruction','qmquote'); ?></th>
        <td><?= esc_html($quotation['instruction']); ?></td>
    </tr>
    <tr>
        <th scope='row'><?php _e('File','qmquote'); ?></th>
        <td><a href='<?php echo $file['url']; ?>' target='_blank'><?php echo $file['url']; ?></a></td>
    </tr>
    <tr>
        <th scope='row'><?php _e('Compatibility','qmquote'); ?></th>
        <td><?php echo $calc['compatibility']; ?></td>
    </tr>
    <tr>
        <th scope='row'><?php _e('Turnaround','qmquote'); ?></th>
        <td><?php echo $calc['turnaround']; ?></td>
    </tr>
    <tr>
        <th scope='row'><?php _e('Number of Page','qmquote'); ?></th>
        <td><?php echo $quotation['quantity']; ?></td>
    </tr>
</table>
<hr />
<h3><?php _e('Breakdown','qmquote'); ?></h3>
<table class='form-table'>
    <?php foreach($calc['breakdown'] as $breakdown) : ?>
    <tr>
        <th style='width:60%' scope="row"><?php echo $breakdown['title']; ?></th>
        <td style='text-align:right'><?= $breakdown['cost']; ?></td>
    </tr>
    <?php endforeach; ?>
    <tr>
        <th><?php _e('Cost Estimation','qmquote'); ?></th>
        <td style='text-align:right'><?php echo $calc['total']; ?></td>
    </tr>
    <tr>
        <th><?php _e('Turnaround Estimation','qmquote'); ?></th>
        <td style='text-align:right'><?php echo $calc['turnaround']; ?></td>
    </tr>
</table>
