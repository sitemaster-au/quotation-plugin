<div class="quoteme-admin-holder">
    <h2>How to</h2>
    <h3>Setup quotation data</h3>
    <ol>
        <li>Make sure you already have account in appquoteme.com</li>
        <li>Go to tab API</li>
        <li>Fill every fields with info that you have ( your account and API key )</li>
        <li>Click <strong>Save Changes</strong> button</li>
        <li>Then, click <strong>Update Data</strong></li>
    </ol>
    <figure>
        <img src="<?php echo QUOTATION_URL; ?>admin/img/quoteme-api.png" alt="">
    </figure>
    <h3>Display quotation form in page</h3>
    <ol>
        <li>Create a page that you want to display your QuoteMeWP services on. Something like 'get-a-quote' or 'quotes'</li>
        <li>Add this shortcode <strong>[quoteme]</strong></li>
        <li>Save the page</li>
    </ol>

    <figure>
        <img src="<?php echo QUOTATION_URL; ?>admin/img/screenshot-quoteme.png" alt="">
    </figure>
</div>
