<div class="quoteme-admin-holder">
    <h2>Upgrade</h2>
    <p>
        Upgrade today and get more storage for your client documents or move up to a team account. Choose a plan
    </p>
    <div class="quoteme-upgrade-holder">
        <div class="upgrade card">
            <header>
                free
            </header>
            <section class='body'>
                <div class="price">
                    <span class='fee-format'>
                        <sup>$</sup>
                        <span class='fee'>0</span>
                    </span>
                    <span class='info'>forever</span>
                </div>
                <ul>
                    <li>100 meg storage</li>
                    <li>Branded</li>
                    <li>Unlimited Quotes</li>
                    <li>Unlimited Services</li>
                </ul>
            </section>
            <footer>
                <a href="#">
                    Sign Up for Free
                </a>
            </footer>
        </div>
        <div class="upgrade card">
            <header>
                starter
            </header>
            <section class='body'>
                <div class="price">
                    <span class='fee-format'>
                        <sup>$</sup>
                        <span class='fee'>10</span>
                    </span>
                    <span class='info'>USD PM / Billed Annually</span>
                </div>
                <ul>
                    <li>300 meg storage</li>
                    <li>White Label</li>
                    <li>Unlimited Quotes</li>
                    <li>Unlimited Services</li>
                </ul>
            </section>
            <footer>
                <a href="#">
                    Start Your 14 Day Trial
                </a>
            </footer>
        </div>
        <div class="upgrade card">
            <header>
                pro
            </header>
            <section class='body'>
                <div class="price">
                    <span class='fee-format'>
                        <sup>$</sup>
                        <span class='fee'>20</span>
                    </span>
                    <span class='info'>USD PM / Billed Annually</span>
                </div>
                <ul>
                    <li>1 gig storage</li>
                    <li>White Label</li>
                    <li>Unlimited Quotes</li>
                    <li>Unlimited Services</li>
                </ul>
            </section>
            <footer>
                <a href="#">
                    Start Your 14 Day Trial
                </a>
            </footer>
        </div>

        <div class="upgrade card">
            <header>
                pro+
            </header>
            <section class='body'>
                <div class="price">
                    <span class='fee-format'>
                        <sup>$</sup>
                        <span class='fee'>50</span>
                    </span>
                    <span class='info'>USD PM / Billed Annually</span>
                </div>
                <ul>
                    <li>3 gig storage</li>
                    <li>White Label</li>
                    <li>Unlimited Quotes</li>
                    <li>Unlimited Services</li>
                </ul>
            </section>
            <footer>
                <a href="#">
                    Start Your 14 Day Trial
                </a>
            </footer>
        </div>

        <div class="upgrade card">
            <header>
                team
            </header>
            <section class='body'>
                <div class="price">
                    <span class='fee-format'>
                        <sup>$</sup>
                        <span class='fee'>250</span>
                    </span>
                    <span class='info'>USD PM / Billed Annually</span>
                </div>
                <ul>
                    <li>10 gig storage</li>
                    <li>White Label</li>
                    <li>Unlimited Quotes</li>
                    <li>Unlimited Services</li>
                </ul>
            </section>
            <footer>
                <a href="#">
                    Start Your 14 Day Trial
                </a>
            </footer>
        </div>
    </div>
</div>
