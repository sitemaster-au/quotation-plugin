<div class="notice notice-info">
    <p>
        <strong><?php _e('Update Service Data','qmquote'); ?></strong><br />
        <?php if('N/A' !== $last_update_service): ?>
        Last update : <strong><?php echo date('d F Y, H:i',$last_update_service); ?></strong><br /><br />
        <?php endif; ?>
        <button id='qmquote-update-service' type="button" name="button" class="button button-primary button-large">
            <span class='message'>Update Data</span>
        </button>
    </p>

</div>
