<?php

namespace QMQUOTE\Admin;

class Connection
{
    protected $token    = NULL;
    protected $valid    = true;
    protected $messages = [];
    // protected $url      = 'https://app.quotemewp.com/wp-json/jwt-auth/v1/token';
    protected $url;

    /**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

    /**
     * Construction
     */
    public function __construct( $plugin_name, $version )
    {
 		$this->plugin_name = $plugin_name;
 		$this->version     = $version;
        $this->url         = QUOTEME_APIURL.'/wp-json/jwt-auth/v1/token';
 	}

    /**
     * Send request to main website to get token
     * @return void
     */
    protected function request_token()
    {
        $username = carbon_get_theme_option('quoteme_account');
        $password = carbon_get_theme_option('quoteme_password');

        if(empty($username)) :
            $this->valid = false;
            $this->messages['error'][]  = __('Your quoteme username is not set','qmquote');
        endif;

        if(empty($password)) :
            $this->valid = false;
            $this->messages['error'][]  = __('Your quoteme password is not set','qmquote');
        endif;

        if(true === $this->valid) :
            $respond = wp_safe_remote_post($this->url,[
                'body'  => [
                    'username'  => $username,
                    'password'  => $password
                ]
            ]);

            $body_respond = json_decode(wp_remote_retrieve_body($respond),true);

            if(isset($body_respond['data']) && 403 === intval($body_respond['data']['status'])) :
                $this->valid = false;
                $this->messages['error'][]  = 'QuoteMe : '.$body_respond['message'];
            else :
                set_transient('qmquote-token',$body_respond['token'],HOUR_IN_SECONDS);
            endif;
        endif;
    }

    /**
     * Check existing token
     * Hooked via action admin_init, priority 999
     * @return void
     */
    public function check_token()
    {
        global $pagenow;

        if(isset($_GET['page']) && 'crb_carbon_fields_container_quoteme_setup.php' === $_GET['page']) :
            do_action('qmquote/connection/check-token');
        endif;
    }

    /**
     * Check JWT token
     * Hooked via action qmquote/connection/check-token, priority 999
     * @return void
     */
    public function check_transient_token()
    {
        global $qmquote;

        $token = get_transient('qmquote-token');

        if(false === $token) :
            $this->request_token();
            $token = get_transient('qmquote-token');
        endif;

        $qmquote['token'] = $token;
    }

    /**
     * Display admin message
     * Hooked via admin_notices, priority 999
     * @return
     */
    public function display_admin_message()
    {
        if(false === $this->valid && isset($this->messages['error']) && 0 < count($this->messages['error'])) :
            ?>
            <div class="notice notice-error is-dismissible">
                <?php foreach((array) $this->messages['error'] as $message) : ?>
                <p><?php echo $message; ?></p>
                <?php endforeach; ?>
            </div>
            <?php
        endif;
    }
}
