(function( $ ) {
	'use strict';

	var qmquote = {
		elements : {
			button : ''
		},
		routine  : {
			'update_service'	: function(){
				$.ajax({
					url : qmquote_var.ajaxurl.update_service,
					dataType : 'json',
					beforeSend : function() {
						qmquote.elements.button.attr('disabled',true)
							.find('.message')
							.html('Updating...')
							.after('<i class="fa fa-spinner fa-spin fa-fw" style="display:inline-block"></i>');
					},
					success : function(response) {
						qmquote.elements.button.attr('disabled',false).find('.message').html('Update Data');
						qmquote.elements.button.find('i.fa').remove();
						confirm(response.message);
					}
				})
			}
		}
	}

	$(document).ready(function(){
		$('#qmquote-update-service').on('click',function(){
			qmquote.elements.button = $(this);
			qmquote.routine.update_service();
		});
	});

})( jQuery );
