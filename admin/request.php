<?php

namespace QMQUOTE\Admin;

class Request
{
    /**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;
	protected $url;
	protected $token;
	protected $messages = [];

    /**
     * Construction
     */
    public function __construct( $plugin_name, $version )
    {
 		$this->plugin_name = $plugin_name;
 		$this->version     = $version;
		$this->url          = [
			'create-quotation' => QUOTEME_APIURL.'/wp-json/quoteme/create-quotation'
		];
 	}

	/**
	 * Get bearer token, if the token is not valid, will request the new one
	 * @return void
	 */
	protected function get_token()
	{
		global $qmquote;

		do_action('qmquote/connection/check-token');

		$this->token = $qmquote['token'];
	}

	/**
	 * Set respond messages
	 * Hooked via filter qmquote/connection/messages, priority 999
	 * @param [type] $messages [description]
	 */
	public function set_messages($messages)
	{
		if(is_array($messages) && 0 < count($messages)) :
			$messages = array_merge($messages,$this->messages);
		else :
			$messages = $this->messages;
		endif;

		return $messages;
	}


    /**
     * Request to create quotation
     * Hooked via action qmquote/request/create-quotation
     * @param  array  $post_data [description]
     * @param  array  $files
     * @return json
     */
    public function create_quotation(array $post_data,array $files)
    {
		global $qmquote;

		$this->get_token();

		if(false === $this->token) :
			$qmquote['messages'][] = __('Cant connect with app.quoteme.com','qmquote');
		else :
			unset($post_data['file']);

			$data = [
				'post' => serialize($post_data),
				'file' => new \CurlFile($files['file']['tmp_name'],$files['file']['type'],$files['file']['name'])
			];

			$post_data['compatibility'] = urlencode(serialize($post_data['compatibility']));
			$post_data['extra']         = urlencode(serialize($post_data['extra']));

			if(isset($files['file']['tmp_name'])) :
				$file = new \CurlFile($files['file']['tmp_name'],$files['file']['type'],$files['file']['name']);
				$post_data['file'] = $file;
			else :
				$post_data['file']	= false;
			endif;

			$ch = curl_init();
			curl_setopt( $ch, CURLOPT_URL, $this->url['create-quotation'] );
			curl_setopt( $ch, CURLOPT_POST,1);
			curl_setopt( $ch, CURLOPT_POSTFIELDS, $post_data);
			curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
			curl_setopt( $ch, CURLOPT_HTTPHEADER, array(
			  "Authorization: Bearer $this->token",
			  "content-type: multipart/form-data;"
			) );

			$response = json_decode(curl_exec( $ch ),1);
			curl_close( $ch );

			$qmquote['rest'] = [
				'response' => $response,
				'post_data'	=> $post_data
			];

			if(false !== $response['success']) :
				$qmquote['rest'] = $response;
			else :
			endif;
		endif;
    }

}
