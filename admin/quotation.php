<?php

namespace QMQUOTE\Admin;

use Carbon_Fields\Container;
use Carbon_Fields\Field;


class Quotation
{
    /**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

    /**
     * Construction
     */
    public function __construct( $plugin_name, $version )
    {
 		$this->plugin_name = $plugin_name;
 		$this->version = $version;
 	}

    /**
     * Register quotation post type
     * Hooked via action init, priority 999
     * @return void
     */
    public function register_post_type()
    {
        $labels = array(
    		'name'               => _x( 'Quotations', 'post type general name', 'qmquote' ),
    		'singular_name'      => _x( 'Quotation', 'post type singular name', 'qmquote' ),
    		'menu_name'          => _x( 'Quotations', 'admin menu', 'qmquote' ),
    		'name_admin_bar'     => _x( 'Quotation', 'add new on admin bar', 'qmquote' ),
    		'add_new'            => _x( 'Add New', 'quotation', 'qmquote' ),
    		'add_new_item'       => __( 'Add New Quotation', 'qmquote' ),
    		'new_item'           => __( 'New Quotation', 'qmquote' ),
    		'edit_item'          => __( 'Edit Quotation', 'qmquote' ),
    		'view_item'          => __( 'View Quotation', 'qmquote' ),
    		'all_items'          => __( 'All Quotations', 'qmquote' ),
    		'search_items'       => __( 'Search Quotations', 'qmquote' ),
    		'parent_item_colon'  => __( 'Parent Quotations:', 'qmquote' ),
    		'not_found'          => __( 'No quotations found.', 'qmquote' ),
    		'not_found_in_trash' => __( 'No quotations found in Trash.', 'qmquote' )
    	);

    	$args = array(
    		'labels'             => $labels,
            'description'        => __( 'Description.', 'qmquote' ),
    		'public'             => false,
    		'publicly_queryable' => false,
    		'show_ui'            => true,
    		'show_in_menu'       => true,
    		'query_var'          => true,
    		'rewrite'            => array( 'slug' => 'quotation' ),
    		'capability_type'    => 'post',
    		'has_archive'        => true,
    		'hierarchical'       => false,
    		'menu_position'      => null,
    		'supports'           => array( 'title')
    	);

    	register_post_type( 'quotation', $args );
    }

    /**
     * Enqueue scripts for quotation
     * @return void
     */
    public function enqueue_scripts()
    {
        global $pagenow,$post;

        if('post.php' === $pagenow && 'quotation' === $post->post_type) :
            wp_enqueue_style('qmquote-quotation'    ,QUOTATION_URL.'/admin/css/qmquote-quotation.css',[],$this->version);
        endif;
    }

    /**
     * Set post meta option
     * Hooked via add_meta_boxes, priority 999
     * @return void
     */
    public function set_metaboxes()
    {
        add_meta_box('qmquote-detail',__('Detail','qmquote'),[$this,'display_metabox'],'quotation');
    }

    /**
     * Display metabox for quotation
     * @return void
     */
    public function display_metabox()
    {
		global $post;

		$quotation = get_post_meta($post->ID,'qmquote',true);
		require_once(plugin_dir_path(__FILE__).'/partials/detail-quotation.php');

    }
}
