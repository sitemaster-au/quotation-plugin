<?php
namespace QMQUOTE\Admin;

use Carbon_Fields\Container;
use Carbon_Fields\Field;

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       https://ridwan-arifandi.com
 * @since      1.0.0
 *
 * @package    Qmquote
 * @subpackage Qmquote/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Qmquote
 * @subpackage Qmquote/admin
 * @author     Ridwan Arifandi <orangerdigiart@gmail.com>
 */
class Service {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;
	protected $file;
	protected $is_setting_page = false;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version     = $version;
		$this->file        = QUOTATION_PATH.'json/data.json';
	}

	/**
	 * Check if current admin page is QuoteMe Setup page
	 * Hooked via action admin_init, priority 999
	 * @return void
	 */
	public function check_if_setting_page()
	{
		if(
			isset($_GET['page']) &&
			'crb_carbon_fields_container_quoteme_setup.php' === $_GET['page']) :
				$this->is_setting_page = true;
		endif;
	}

	/**
	 * Set CSS and JS files
	 * Hooked via action admin_enqueue_scripts, priority 999
	 * @return void
	 */
	public function set_css_js_scripts()
	{
		if(false !== $this->is_setting_page) :
			wp_enqueue_style	('fontawesome-v4');
			wp_enqueue_script	($this->plugin_name);
		endif;
	}

	/**
	 * Display admin message to update service data
	 * Hooked via action admin_notices
	 * @return void
	 */
	public function show_update_dialog()
	{
		if(false !== $this->is_setting_page) :
			$last_update_service	= get_option('qmquote-last-update','N/A');
			require QUOTATION_PATH.'admin/partials/update-dialog.php';
		endif;
	}

	/**
	 * Write json data
	 * @param  array  $json_data [description]
	 * @return [type]            [description]
	 */
	protected function write_to_file(array $json_data)
	{
		if(is_array($json_data) && 0 < count($json_data)) :

			$write_data = [];

			foreach($json_data as $id => $data) :
				$write_data[$id] = [
					'ID'            => $data['ID'],
					'title'         => $data['post_title'],
					'image'         => $data['meta_data']['setting']['logo'],
					'color'         => $data['meta_data']['setting']['color'],
					'compatibility' => $data['meta_data']['compatibility'],
					'pt'            => [],
					'additionals'   => [],
					'other'         => [
						'discount'	=> [
							'active'	=> (false !== $data['meta_data']['discount']['active']) ? 'active' : '',
							'amount'	=> $data['meta_data']['discount']['amount'],
							'text'		=> $data['meta_data']['discount']['text'],
							'highlight' => $data['meta_data']['discount']['highlight']
						]
					]
				];

				$additionals = $data['meta_data']['additionals'];

				if(is_array($additionals) && 0 < count($additionals)) :
					foreach($additionals as $add) :
						$write_data[$id]['additionals'][]	= [
							'title'           => $add['name'],
							'price'           => $add['cost'],
							'express-pricing' => $add['express'],
							'deliver'         => [
								'standard' => $add['standard_delivery'],
								'express'  => $add['express_delivery'],
							]
						];
					endforeach;
				endif;

				$turnarounds = $data['meta_data']['turnaround'];

				if(is_array($turnarounds) && 0 < count($turnarounds)) :
					foreach($turnarounds as $turn) :
						$write_data[$id]['pt'][]	= [
							'pricing'		=> [
								'desktop' => $turn['desktop'],
								'tablet'  => $turn['tablet'],
								'mobile'  => $turn['mobile'],
								'other'   => $turn['other'],
							],
							'express-pricing' => $turn['express'],
							'delivery'        => [
								'standard' => $turn['standard_delivery'],
								'express'  => $turn['express_delivery'],
							]
						];
					endforeach;
				endif;
			endforeach;

			if(!is_writeable($this->file)) :
				chmod($this->file,0755);
			endif;

			file_put_contents($this->file,json_encode($write_data));
			update_option('qmquote-last-update',current_time('timestamp'));
		endif;
	}

    /**
     * Update json via rest
     * Hooked via action quoteme/rest/service/update-data
     * @return void
     */
    public function update_json_via_rest()
    {
        $push_data  = wp_parse_args($_POST,[
            'key'   => NULL,
            'data'  => NULL
        ]);

        $push_api_key = trim($push_data['key']);
        $site_api_key = trim(carbon_get_theme_option('quoteme_api_key'));

        if($push_api_key === $site_api_key) :

			$this->write_to_file($push_data['data']);

			echo wp_send_json([
				'update-json'	=> current_time('mysql')
			]);
			exit;
        endif;

		echo wp_send_json([
			'update-json'	=> false
		]);
		exit;
    }

	/**
	 * Update json via ajax
	 * Hooked via action, wp_ajax_request_update_service
	 * @return json
	 */
	public function update_json_via_ajax()
	{
		global $qmquote;
		$total_data = 0;
		do_action('qmquote/connection/check-token');

		$token 	= $qmquote['token'];

		$url      = QUOTEME_APIURL.'/wp-json/quoteme/push-service';
		$response = wp_safe_remote_post($url,[
			'headers'	=> [
				'Authorization' => 'Bearer '.$token
			]
		]);

		$body_respond = json_decode(wp_remote_retrieve_body($response),true);

		if(isset($body_respond['push']['data']) && 0 < count($body_respond['push']['data'])) :
			$this->write_to_file($body_respond['push']['data']);
			$message = sprintf(__('Service data updated. Found %d data','qmtuote'),count($body_respond['push']['data']));
		else :
			$message = __('Can\t update the data. Make sure that you fill the API info with right value','qmquote');
		endif;

		echo wp_send_json([
			'success'	=> true,
			'response'	=> json_decode(wp_remote_retrieve_body($response)),
			'message'	=> $message
		]);
		exit;
	}
}
