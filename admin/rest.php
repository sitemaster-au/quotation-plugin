<?php

namespace QMQUOTE\Admin;

class Rest
{
    private $key        = 'quoteme';        // change this prefix
    private $script     = 'quoteme-action'; // change this script action
    /**
     * Construction
     */
    public function __construct()
    {

    }

    /**
     * Register rewrite url
     * Hooked via action init, priority 1
     * @return void
     */
    public function register_rewrite_url()
    {
        // enable rest request without value
        add_rewrite_rule('^'.$this->key.'/([^/]*)/([^/]*)/?',
               'index.php?script='.$this->script.'&controller=$matches[1]&action=$matches[2]&value=0',
               'top'
           );

        // rest request with value
        add_rewrite_rule('^'.$this->key.'/([^/]*)/([^/]*)/([^/]*)/?',
               'index.php?script='.$this->script.'&controller=$matches[1]&action=$matches[2]&value=$matches[3]',
               'top'
           );
    }

    /**
     * Register custom query vars
     * Hooked via filter query_vars, priority 999
     * @param  array $vars
     * @return array
     */
    public function register_query_vars($vars)
    {
        $vars[] = 'script';
        $vars[] = 'controller';
        $vars[] = 'action';
        $vars[] = 'value';

        return $vars;
    }

    /**
     * Check rest url API
     * Hooked via template_redirect, priority 1
     * @return void
     */
    public function check_request_url()
    {
        global $wp_query;

        if(isset($wp_query->query['script']) && $this->script === $wp_query->query['script']) :

            $controller = $wp_query->query['controller'];
            $action     = $wp_query->query['action'];
            $value      = $wp_query->query['value'];
            header("HTTP/1.1 200 OK");
            do_action($this->key.'/rest/'.$controller.'/'.$action,$value);
            exit;
        endif;
    }
}
